# Tips for lab 04


## Debug script for static loaded library


```
#!/bin/bash

make clean

make lab4

./lab4
./lab4 1
./lab4 1 1
./lab4 1 2 3
```


## Debug script for dynamic loaded library


```
#!/bin/bash

make clean

make lab4_dyn

export LD_LIBRARY_PATH=lib:$LD_LIBRARY_PATH

./lab4_dyn
./lab4_dyn 1
./lab4_dyn 1 1
./lab4_dyn 1 2 3

```

## Expected output

```
Error 0 found in ohno_test_app - level = FATAL - there's no arguments!
Error 0 found in ohno_test_app - level = SERIOUS - get_sd given single value
Error 1 found in ohno_test_app - level = WARNING - standard deviation is zero, are those really random numbers?
mean: 1.000000
stdev: 0.000000
Error 0 found in ohno_test_app - level = WARNING - standard deviation is zero, are those really random numbers?
mean: 1.000000
stdev: 0.000000
mean: 2.000000
stdev: 0.816497
```


## Turn in

Please turn in following files and folders.

```
uoregon-cis330-s20
|-lab04
  |-include
    |-ohno.h
	|-stats.h
  |-Makefile
  |-README
  |-src
    |-lab4.c
	|-ohno.c
	|-stats.c
```

